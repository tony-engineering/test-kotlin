import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MostFreeTime(input: ArrayList<String>) {

    private val TIME_PATTERN = "hh:mma"
    private val TIME_PATTERN_LOCALE = Locale.ENGLISH

    var input = input
    private val freeSlots = arrayListOf<Int>()
    private var mostFreeTime: Int = 0
    var output = String()

    init {
        orderInput()
    }

    fun orderInput(): MostFreeTime {
        val beginTimeCompatator = Comparator { str1: String, str2: String ->
            (getBeginDate(str1).time - getBeginDate(str2).time).toInt()
        }
        input = ArrayList(input.sortedWith(beginTimeCompatator))
        return this
    }

    fun compute(): MostFreeTime {
        computeFreeSlots()
        identifyMostFreeTime()
        formatOutput()
        return this
    }

    private fun computeFreeSlots() {
        for(i in 0 until input.size-1) {
            val currentBusySlot = input[i]
            val nextBusySlot = input[i+1]
            freeSlots.add(freeSlotBetween(currentBusySlot, nextBusySlot))
        }
    }

    private fun freeSlotBetween(currentBusySlot: String, nextBusySlot: String): Int {
        return minutesBetween(getEndDate(currentBusySlot), getBeginDate(nextBusySlot))
    }

    private fun minutesBetween(endDateOfCurrentBusySlot: Date, beginDateOfNextBusySlot: Date): Int {
        return ((beginDateOfNextBusySlot.time/1000 - endDateOfCurrentBusySlot.time/1000) / 60).toInt()
    }

    private fun getEndDate(busySlot: String): Date {
        val currentBeginAndEndDates = extractBeginAndEndDates(busySlot)
        return currentBeginAndEndDates.second
    }

    private fun getBeginDate(busySlot: String): Date {
        val currentBeginAndEndDates = extractBeginAndEndDates(busySlot)
        return currentBeginAndEndDates.first
    }

    private fun extractBeginAndEndDates(busySlot: String): Pair<Date, Date> {
        val formatter = SimpleDateFormat(TIME_PATTERN, TIME_PATTERN_LOCALE)
        val begin = formatter.parse(busySlot.substring(0, 7))
        val end = formatter.parse(busySlot.substring(8, 15))
        return Pair(begin, end)
    }

    private fun identifyMostFreeTime() {
        mostFreeTime = freeSlots.max()!!
    }

    private fun formatOutput() {
        val hours: Int = mostFreeTime / 60
        val minutes: Int = mostFreeTime % 60
        output = String.format("%02d:%02d", hours, minutes)
    }
}
