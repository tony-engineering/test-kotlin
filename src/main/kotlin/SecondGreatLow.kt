class SecondGreatLow(input: IntArray) {

    private val input: IntArray = input
    val output: IntArray = IntArray(2)

    fun arrayChallenge(): SecondGreatLow {
        output[0] = input[1]
        output[1] = input[input.size-2]
        return this
    }
}
