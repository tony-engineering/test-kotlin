fun main() {
    println("Sample execution of SecondGreatLow:")
    println("input: [1, 42, 42, 180]")
    val input1 = intArrayOf(1, 42, 42, 180)
    val output1 = SecondGreatLow(input1).arrayChallenge().output.joinToString(", ")
    println("output1: $output1")

    println("-------------------------")

    println("Sample execution of MostFreeTimeClass:")
    println("input: [\"09:10AM-09:50AM\", \"10:00AM-12:30PM\", \"02:00PM-02:45PM\"]")
    val input2 = arrayListOf("09:10AM-09:50AM", "10:00AM-12:30PM", "02:00PM-02:45PM")
    val output2 = MostFreeTime(input2).compute().output
    println("output: $output2")
}